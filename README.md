# docker-ssh-tunnel-example

内部ネットワークで動かすWebアプリケーションを、sshのポートフォワーディングを通じて、パブリックインターネットからアクセスできるようにするサンプル。

また、パブリック側は`http://example.com/推測されにくい文字列/`という文字列でしかアクセスできないようにする。
応用でBasic認証などを設定することも可能。

- `app-server`: 内部ネットワークで動かすアプリケーション
    - 構成
        - アプリケーション
        - nginx
        - `public-server`へsshでリモートフォワードするためのsshクライアント
- `public-server`: インターネットから`app-server`にアクセスできるようにするsshトンネル
    - 構成
        - nginx
        - `app-server`からのssh接続を待ち受けるsshサーバ

`app-server`へはsshも含めて外部からアクセスできない想定。
よって、`app-server`側から`public-server`側へssh接続を行い、リモートフォワードによってトンネリングする。

![diagram](./docker-ssh-tunnel-example.drawio.svg)

## サンプルの動かし方

### 鍵の作成

ssh用の鍵を作成する。

```sh
mkdir keys
ssh-keygen -t rsa -b 4096 -f keys/id_rsa
```

秘密鍵を`app-server/ssh`にコピーする。

```sh
mkdir app-server/ssh
cp keys/id_rsa app-server/ssh/
```

公開鍵を`public-server/ssh`にコピーする。

```sh
mkdir public-server/ssh
cp keys/id_rsa public-server/ssh/
```

### public-serverの設定

- `public-server/docker-compose.yml`
    - コメントを参考にしながら適宜書き換える。
- `public-server/nginx/conf.d/default.conf`
    - コメントを参考にしながら適宜書き換える。
    - `server_name`, `location`, `proxy_pass`のあたり。

### public-serverの起動

設定が済んだら、`docker-compose`で起動する。

```sh
docker-compose up -d
```

### app-serverの設定

- `app-server/docker-compose.yml`
    - コメントを参考にしながら適宜書き換える。

### app-serverの起動

設定が済んだら、`docker-compose`で起動する。

```sh
docker-compose up -d
```

### 動作確認

`curl`やブラウザでアクセスしてみる。

デモ設定のままの場合:

- `public-server`へのアクセス
    - http://localhost/foobar/baz?param=123
- `app-server`へのアクセス
    - http://localhost:8080/baz?param=123

またsshトンネルの自動復旧についても確認する。

- `public-server`を終了する（`docker-compose down`）
- しばらくしてから再度`public-server`を起動
- `app-server`のログを確認する（`docker-compose logs -f）
    - sshの接続エラーと、`ssh-tunnel`の再起動、再接続を確認する

## 応用

- `public-server`のnginxでBasic認証などを設定する。
