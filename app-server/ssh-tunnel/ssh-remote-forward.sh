#!/bin/bash

set -eu

mkdir /root/.ssh && chmod 700 /root/.ssh
cp -r /ssh/* /root/.ssh/ \
    && chmod 600 /root/.ssh/*

ssh -p "${SSH_PORT:-22}" \
    -l "${SSH_USER}" \
    -i "/root/.ssh/${PRIVATE_KEY_FILE:-id_rsa}" \
    -o "StrictHostKeyChecking=no" \
    -o "UserKnownHostsFile=/dev/null" \
    -o "ServerAliveInterval=${SERVER_ALIVE_INTERVAL:-60}" \
    -o "ExitOnForwardFailure=yes" \
    -N \
    -T \
    -R "${REMOTE_HOST:-0.0.0.0}:${REMOTE_PORT}:${DEST_HOST:-127.0.0.1}:${DEST_PORT}" \
    "${SSH_HOST}"
