const express = require("express");

const port = Number(process.env["SERVER_PORT"] || 3000);
const app = express();

app.get("/*", (req, res) => {
  const content = {
    method: req.method,
    headers: req.headers,
    path: req.path,
    query: req.query
  }
  // res.json(content);
  res.type("application/json").send(JSON.stringify(content, null, 2));
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
