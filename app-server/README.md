# app-server

内部ネットワークで公開するアプリケーションサーバ群。

- `myapp`: Webアプリケーション（のサンプル）
- `ssh-tunnel`: リモートフォワードするsshクライアント
- `ssh`: ssh用の鍵を入れておくディレクトリ

## 鍵の作成

`./ssh/id_rsa`に作成する。

```sh
mkdir ssh
ssh-keygen -t rsa -b 4096 -f ./ssh/id_rsa
```
