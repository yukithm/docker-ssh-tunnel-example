# public-server

インターネットから`app-server`へアクセスさせるためのプロキシ。

- `ssh-server`: sshサーバ（`app-server`の`ssh-tunnel`からの接続を受ける）
- `nginx`: nginxの設定ファイル
- `ssh`: ssh用の鍵を入れておくディレクトリ

## ssh用の鍵

`app-server`のほうで作成した公開鍵(`id_rsa.pub`)を入れておく。
